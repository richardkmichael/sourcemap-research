function boldLastName(strings, firstName, lastName) {
  return `${strings[0]} ${firstName} <b>${lastName}</b>`
}

let firstName = "Scooby"
let lastName = "Doo"
let content = boldLastName`Hello, ${firstName} ${lastName}`

document.addEventListener("DOMContentLoaded", function() {
  document.getElementById("change-text").addEventListener("click", function() {
    document.getElementById("text").innerHTML = content
  })
})
